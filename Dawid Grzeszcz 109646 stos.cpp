﻿
#include <iostream>
#include <ctime>
using namespace std;
class Stack // declaraction of a class
{
public:
int tab[100];
int i;
void init()
{
	this->i = 0;
}
void destroy() // empty due to solution used.
{
}
void push(int p) // p stands for push which should be the number you want to get on top of the stack.
{
	this->tab[this->i] = p;
	this->i++;
}
void pop() // taking top number off the stack by reducing i.
{
	if (this->i != 0) this->i--; // countermeasure to prevent i  going into negative values.
}
int top() // returning the value of current index.
{
	return this->tab[this->i-1]; // -1 due to error found during debugging process whereas push() function in current formula actually makes it exceed range
}
bool empty() // checking if first index is empty.
{
	if (this->i==0) return 1;
	return 0;
}
bool full() // checking if last index is empty.
{
	if (this->i==100) return 1;
	return 0;
}
};
int main()
{
	Stack test;
	test.init();
	srand(time(NULL));
	int x =(rand() % 100)+1 ; // randomizing the amount of numbers to be pushed onto the stack (1-100)
	for (int i = 0; i < x; i++)
	{
		test.push(rand() % 1000); // placing a random number(0-1000) onto the top of stack X(previously randomized number) times.
	}
	while (test.empty()==0) // taking numbers off the stack until it is empty while outputing them to console screen.
	{
		cout << test.top()<<endl;
		test.pop();
	}
}
